/**
 * Created by piotrostiak on 14/04/14.
 */
var CollectionView = Backbone.View.extend({
    el: "#CollectionView",
    template: Handlebars.compile($("#collection-item-template").html()),
    items: [],
    itemWidth: 0,
    itemHeight: 0,
    collectionScroll: 0,
    bLoaded:false,
    bReady: false,
    selectedItemID:0,
    hoverTimeout:null,
    events: {
        'mousemove': 'onMouseMove',
        'mouseover .collection-item':'mouseEnter',
        'mouseout .collection-item':'mouseLeave',
        'touchmove':'onTouchMove',
        'click .collection-item':'itemClicked'
    },
    initialize: function () {
        this.render();
    },
    render: function () {
        var i;

        this.items.push([], [], [], [], []);

        this.$el.html(""); //app.collectionData.toString());
        var currentColumn = 0;

       // for (i = 1; i <= 5; i++) {
            this.$el.append('<div id="whiteStripe' + 0 + '" class="whiteCollectionStripe"/>');
       // }
        for (i = 1; i <= app.collectionData.length; i++) {
            var imageFile = (i);

            if (i < 10) imageFile = "0" + i;

            this.$el.append(this.template({id: i, imageURL: "assets/img/collection/" + imageFile + ".jpg", skuName:app.collectionData[i-1].sku_name}));
            this.items[currentColumn].push($('#collectionItem' + i));

            currentColumn++;

            if (currentColumn >= 5)
                currentColumn = 0;
        }


        this.onResize();
        this.$el.hide();
        //   this.slideIn();
    },
    onResize: function () {

        var viewWidth = this.$el.width();
        var itemRatio = 0.68181818181;
        this.itemWidth = Math.ceil(viewWidth / 5);
        this.itemHeight = Math.round(this.itemWidth * itemRatio);
        $('.collection-item').css({'width': this.itemWidth, 'height': this.itemHeight});
   //     $('.collection-item:hover .sku-name').css({y:this.itemHeight*0.5 - 9});
        this.reposition();
    },
    reposition: function () {
        var i = 0;
        var col = 0;

        $('#whiteStripe0' ).css({left:0, width: '100%', height:'100%'});

        for (col = 0; col < 5; col++) {

            for (i = 0; i < this.items[col].length; i++) {
                this.items[col][i].css({top: Math.round(this.itemHeight * i), left: this.itemWidth * col });
            }

        }

        $('.collection-item').css({'width': this.itemWidth, 'height': this.itemHeight});


    },
    open:function(){
        window.location.hash = '!collection';
    },
    slideIn: function () {
        // top:-100%;
        this.$el.show();

        // window.location = 'collection';
        var me = this;
        setTimeout(function () {
            app.slideShowView.hide();
            /*$('.whiteCollectionStripe').hide(); */
            me.bReady = true;
            me.bLoaded = true;
        }, 1500);

        this.initSmoothSlide();
        this.$el.css({y: '-100%'});
        this.smoothSlide(0, 2.3);
    },
    slideOut: function () {
        // top:-100%;
        var me = this;


      this.$el.transition({y:this.$el.height() },1000, function(){ me.$el.hide();});

    },
    onMouseMove: function (e) {
        if (this.bReady == false) return;
        var mouseY = (e.originalEvent.pageY - 50) / (this.$el.height());
        if(mouseY > 1) mouseY = 1;
        if(mouseY < 0 ) mouseY =0;
        this.smoothSlide(mouseY, 1.3);
    },
    onTouchMove:function (e) {
        e.stopPropagation();e.preventDefault();
    if (this.bReady == false) return;

        var newPosition = e.originalEvent.touches[0].pageY;
    var mouseY = (newPosition - 50) / (this.$el.height());


    this.smoothSlide(mouseY, 2.3);
},
    smoothSlide: function (to, timeScale) {
        var c = [0.5, 0.6 , 0.7, 0.8, 0.9 ];

        var pos = parseInt((  this.itemHeight * this.items[0].length - this.$el.height() ) * -to);

        for (col = 0; col < 5; col++) {
            for (i = 0; i < this.items[col].length; i++) {
                TweenLite.to(this.items[col][i], c[col] * timeScale, {y: Math.round(pos), force3D: true});

            }
        }
    },
    initSmoothSlide: function () {


        var pos = parseInt((this.$el.height() - this.itemHeight - 50 ) * 3);
        $('#whiteStripe0').css({top: this.$el.height()});
        TweenLite.to($('#whiteStripe0'),1, {top: 0, force3D: true});
        for (col = 0; col < 5; col++) {
            var c = [0.5, 0.6 , 0.7, 0.8, 0.9 ];


            for (i = 0; i < this.items[col].length; i++) {
                TweenLite.to(this.items[col][i], 0, {y: pos, force3D: true});

            }
        }
    },
    itemClicked:function(e){
        if( !this.bReady ) return;
        var itemID = parseInt(e.currentTarget.id.replace("collectionItem", "")-1);
        this.selectedItemID = itemID;

        $(".collection-item").css( "pointer-events","none");

        this.$el.find('#'+e.currentTarget.id).addClass("selected");

      //  app.productView.open(itemID,'current');
        this.bReady = false;
    },
    openProductView:function(itemID){
        this.selectedItemID = itemID;

        $(".collection-item").css( "pointer-events","none");
        app.productView.open(itemID,'current');
        this.bReady = false;

    },
    backFromProductView:function(bDontOpen){
    // alert('#collectionItem'+this.selectedItemID);
        this.$el.find(".collection-item").css( "pointer-events","all");
        this.$el.find('#collectionItem'+parseInt(this.selectedItemID)).removeClass("selected");

        if(bDontOpen != true)
        this.open();
    },
    mouseEnter:function(e){

     this.hoverTimeout = setTimeout(function() {
         $(e.currentTarget).addClass('collection-item-hover');
     },200);

    },
    mouseLeave:function(e){

        $(e.currentTarget).removeClass('collection-item-hover');
        clearTimeout(this.hoverTimeout );
    }
});