/**
 * Created by piotrostiak on 14/04/14.
 */
var DetailsView = Backbone.View.extend({
    el:"#DetailsView",
    template: Handlebars.compile($("#details-view-template").html()),
    detailsData:null,
    bOpened:false,
    productID:0,
    slidingIn:false,
    initialize:function(){
        this.$el.hide();

    },
    render:function(){
        var me = this;
 
        this.detailsData = {
            imageURLA:"assets/img/details/details01_a.jpg",
            imageURLB:"assets/img/details/details01_b.jpg",

            titleA:"SOME TITLE COMES HERE "+ this.productID,
            textA:"Lorem dolor psum sit amet fluctuat nec mergitur sic itur ad astra, per angusta ad augusta. Tu quoque mi filii, gaius julius said.",

            titleB:"SOME TITLE COMES HERE" + this.productID,
            textB:"Lorem dolor psum sit amet fluctuat nec mergitur sic itur ad astra, per angusta ad augusta. Tu quoque mi filii, gaius julius said."
        };

        $(document).smartpreload({ images:  [  me.detailsData.imageURLA,  me.detailsData.imageURLB ], onloadall: function () {
            me.$el.html(me.template(me.detailsData));
                if(me.bOpened == false){
                    me.$el.css({y:app.productView.$el.height()});
                    me.$el.show();


                }
           // app.productView.slideDetails(true);

             $(' #detailsParagraphA, #detailsParagraphB,#detailsHeaderA, #detailsHeaderB').css({opacity:0});

       //     me.$el.hide();
        }});

    this.bOpened = false;

    },

    slideInComplete:function(){
        var me = this;
         setTimeout(function(){   me.slideInElements();}, 400);
        me.bOpened = true;
    },
    slideInElements:function(){

        var me = this;

       // $('.columnA, .columnB').transition({opacity:1},600);
       $('#detailsImageA').transition({scale:1.1}, 40000);
        $('#detailsImageB').transition({scale:1.1}, 40000);

        setTimeout(function(){me.slidingIn= false;app.productView.bDetailsSliding = false;}, 650);
        if( $('#detailsHeaderA').css('opacity') == 0)
        $('#detailsHeaderA').css({ y:'-60px'});
        $('#detailsHeaderA').transition({opacity:1, y:'0px', delay:0},600);
        $('#detailsParagraphA').transition({opacity:1, delay:600},1000);

        if( $('#detailsHeaderB').css('opacity') == 0)
        $('#detailsHeaderB').css({  y:'-60px'});
        $('#detailsHeaderB').transition({opacity:1, y:'0px', delay:1200},600);
        $('#detailsParagraphB').transition({opacity:1, delay:1800},1000);

        setTimeout(function(){me.slidingIn= false; }, 1000);
    },
    slideIn:function(productID){

        if(this.slidingIn == true)
        return;
        this.slidingIn = true;

        this.productID = productID;

  //      this.render();


    },
    slideOut:function(){


        if(this.slidingIn == true)
            return;
        this.slidingIn = true;

        var me = this;
        this.$el.show();

        setTimeout(function(){me.slidingIn= false; app.productView.bDetailsSliding = false;}, 1000);
    //    this.$el.transition({y:app.productView.$el.height()},600,'easeInOutSine', function(){   me.$el.hide(); });
        me.bOpened = false;
    }
});