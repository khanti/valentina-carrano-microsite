/**
 * Created by piotrostiak on 14/04/14.
 */
var ProductView = Backbone.View.extend({
    el: "#ProductView",
    template: Handlebars.compile($("#product-page-template").html()),
    productID: 0,
    previousProductID:0,
    currentAltImage: 1,

    bZoomReady: false,
    tempID: 0,
    startX: 0,
    startY: 0,
    currentMouseX: 0,
    currentMouseY: 0,
    lastZoomPosX: 0,
    lastZoomPosY: 0,
    currentScrollDelta:0,

    activeSlide:"",
    detailsView:null,

    bDetailsShown:false,
    bDetailsSliding:false,
    bSlidePlaying: false,

    direction:1,
    contentHeight:0,
    initialize: function () {
        this.$el.hide();
        var me = this;
     //   setInterval(function(){ me.onTick();},100);
       // _.bindAll(this);
    },
    events: {
        'click .productLeftArrow': 'onLeftClicked',
        'click .productRightArrow': 'onRightClicked',
        'click .productDetailsButton': 'onDetailsButtonClicked',
        'click #product-slider-pages > i': 'pageDotClicked',
        'click #backToCollection': 'onBackToCollection',
        'click .productImage': 'onImageClicked',
        'click .hd-image-wrapper': 'onZoomedImageClicked',
        'mousemove .hd-image-wrapper': 'onMouseZoomMove',
        'DOMMouseScroll': 'onMouseWheel',
        'mousewheel': 'onMouseWheel'

    },
    render: function (activeSlide) {

        this.activeSlide = activeSlide;
        if(activeSlide == "current") {
            this.$el.html(this.template({
                imageURL1: "assets/img/products/normal/product" + this.tempID + "_1.jpg",
                SKUName:app.collectionData[this.productID-1].sku_name

            }));

        }
        else{

            $('.slideshow-wrapper-'+activeSlide+ '> .product-sku-name').html(app.collectionData[this.productID-1].sku_name);
       //    $('.slideshow-wrapper-current > .product-sku-name').removeClass('fade-in');
           // $('.slideshow-wrapper-new > .product-sku-name').addClass('fade-in');
        }


        $('.slideshow-wrapper-'+activeSlide+ '> .productImage1').css({'background-image':'url('+"assets/img/products/normal/product" + this.tempID + "_"+this.currentAltImage+".jpg"+')',
        opacity:1});

        if(activeSlide == "current"){
            $('.slideshow-wrapper-new').hide();
            $('.product-sku-name-new').hide();

            this.detailsView = new DetailsView();
            this.detailsView.productID = this.productID;
            this.detailsView.render();this.zoomIn();
        }

        else{
            $('.slideshow-wrapper-new').show();
            this.slideInSlide();
            this.detailsView.productID = this.productID;
            this.detailsView.render();
        }



        this.contentHeight = this.$el.height();
        return this;
    },
    open: function (productID, activeSlide) {
        var me = this;

        this.productID = productID;

        this.tempID = this.productID + 1;
        this.tempID = 1;

        if (this.tempID < 10)
            this.tempID = "0" + this.tempID;
        this.currentAltImage = 1;

        this.$el.find('#product-slider-pages > i').removeClass('active');
        this.$el.find('#productDot' + 1).addClass('active');
        $(document).smartpreload({ images:  [ "assets/img/products/normal/product" + this.tempID + "_"+this.currentAltImage+".jpg" ], onloadall: function () {
            me.render(activeSlide);
        }});




    },
    moveToSlide: function (slideIndx) {
        $('.product-sku-name').removeClass('fade-in');
        TweenLite.to($('.product-sku-name'),0.65,{opacity:0, y:this.$el.height()*-0.5, ease:"Power1.easeOut"});
        this.bSlidePlaying = true;
        //app.productView.productID  =slideIndx;
        this.previousProductID = (this.productID );
        window.location.hash = '!product/'+(slideIndx );
        //alert(     this.previousProductID + " " + this.productID);
        this.open(slideIndx, "new");

       if(this.detailsView.bOpened)
       this.detailsView.slideIn(slideIndx);

    },
    slideInSlide:function(){
        var slideWidth = Math.floor(this.$el.find('.productImage').width() + 2);
      
        var me = this;
        var slideIndx =  this.productID;

      //  if( this.productID > this.previousProductID)
        //    this.direction = 1;

        this.$el.find( '.slideshow-wrapper-new').css({x: -1*this.direction* slideWidth});


        this.$el.find( '.slideshow-wrapper-new').transition({x: 0}, 1000, 'easeInOutSine');

        this.$el.find( '.slideshow-wrapper-current').transition({x: this.direction * slideWidth}, 1000, 'easeInOutSine', function(){
            var newW = $( '.slideshow-wrapper-new');
            var newC = $( '.slideshow-wrapper-current');
            newW.removeClass('slideshow-wrapper-new');
            newC.removeClass('slideshow-wrapper-current');

            newC.addClass('slideshow-wrapper-new');
            newW.addClass('slideshow-wrapper-current');
            $('.product-sku-name').html(app.collectionData[me.productID-1].sku_name);

            TweenLite.to($('.product-sku-name'),0.65,{opacity:0.3, y:0,  ease:"Power1.easeOut"});

            me.bSlidePlaying = false;
            //     me.skuNameFadeIn("current");
           //  if(this.activeSlide != "current")
            // $('.slideshow-wrapper-current > .product-sku-name').addClass('fade-in');

        });
    },
    slideDetails:function(bUp){
        var direction = -1;

        if(bUp)
        {
            this.$el.find( '.slideshow-wrapper-current').transition({y:parseInt(direction*20)+'%'},1000,'cubic-bezier(0.165, 0.84, 0.44, 1)' );
          //  this.$el.find( '.product-sku-name').transition({y:parseInt(direction*1500)+'%',opacity:0}, 800, 'easeInOutSine');
            this.$el.find( '.product-sku-name').addClass('slide-up');

        }
        else{
            this.$el.find( '.slideshow-wrapper-current').transition({y: '0%'}, 1000,'cubic-bezier(0.165, 0.84, 0.44, 1)');
           // this.$el.find( '.product-sku-name').transition({y:'0%',opacity:0.3}, 1000, 'easeInOutSine');
            this.$el.find( '.product-sku-name').removeClass('slide-up');
        }

    },

    /*
     Zooms in the product view from collection view
     */
    zoomIn: function () {
        this.bSlidePlaying = true;
        var scaleRatio = app.collectionView.itemWidth / this.$el.width();
        var me = this;
        var scrollY = 0;
        if($("#collectionItem" + (this.productID + 1))[0] != undefined&&$("#collectionItem" + (this.productID + 1))[0]._gsTransform != undefined)
        scrollY = $("#collectionItem" + (this.productID + 1))[0]._gsTransform.y;
            var itemY = parseInt($("#collectionItem" + (this.productID + 1)).css('top')) +scrollY;

            this.startX = (parseInt($("#collectionItem" + (this.productID + 1)).css('left')) - this.$el.width() / 2 + app.collectionView.itemWidth / 2);//-this.$el.width()/2;
            this.startY = (itemY - this.$el.height() / 2 + app.collectionView.itemHeight / 2);//-this.$el.width()/2;



        this.$el.find('.productImage').css({scale: scaleRatio});
        this.$el.css({ x: this.startX, y: this.startY, opacity: 0, scale: 1  });


        this.$el.show();

        this.$el.find('.productImage').transition({scale: 1, opacity: 1}, 600, "easeOutCubic");
        this.$el.transition({  x: 0, y: 0, opacity: 1 }, 600, "easeOutCubic", function () {
            me.slideInUI();
            me.bSlidePlaying = false;
            $("#CollectionView").hide();
        });


    },
    /*
     Zooms out to the collection grid
     */
    zoomOut: function (bDontShow) {
        var scaleRatio = app.collectionView.itemWidth / this.$el.width();
        var me = this;
        if(bDontShow != true)
        $("#CollectionView").show();
        this.slideOutUI();
        //  this.$el.find('.productImage').transition({scale:scaleRatio, opacity:0 },1000,"easeOutCubic");
        this.$el.transition({ x: this.startX, y: this.startY, scale: scaleRatio, opacity: 0}, 1000, "easeOutCubic", function () {
            me.$el.hide();
            app.collectionView.bReady = true;
        });
    },

    moveToAltView: function (slideIndx) {
        var me = this;
        var previousAltImage   = me.currentAltImage;

        var slideWidth = Math.floor(this.$el.find('.productImage').width() + 2);
        this.$el.find('#product-slider-pages > i').removeClass('active');
        this.$el.find('#productDot' + slideIndx).addClass('active');


        $(document).smartpreload({ images: [ "assets/img/products/normal/product" + me.tempID + "_"+slideIndx+".jpg"],  onloadall: function () {

            me.$el.find('.slideshow-wrapper-current > .productImage'+slideIndx).css({'background-image':'url('+"assets/img/products/normal/product" + me.tempID + "_"+slideIndx+".jpg"+')',
            'opacity':0, scale:1.2,'display':'block'});

            me.$el.find('.slideshow-wrapper-current > .productImage'+previousAltImage).transition({opacity:0}, 1000, 'easeInOutSine',function(){
                 me.$el.find('.slideshow-wrapper-current > .productImage'+previousAltImage).hide();
            });
            me.$el.find('.slideshow-wrapper-current > .productImage'+slideIndx).transition({opacity:1,scale:1}, 1000, 'easeInOutSine');
            me.currentAltImage = slideIndx;
        }});
        
     //   this.$el.find('.productImage').transition({x: -(slideIndx - 1) * slideWidth}, 1000, 'easeInOutSine');
    },

    slideInUI: function () {
        $(".productLeftArrow,.productRightArrow,.productDetailsButton,.productFB,.productTwitter,.productInstagram").addClass('fade-in');
      //  $('.slideshow-wrapper-'+this.activeSlide+ '> .product-sku-name').transition({y:})
       // if(this.activeSlide == "current")
         this.skuNameFadeIn("current");
     // $('.product-sku-name').addClass('fade-in');
        TweenLite.to($('.product-sku-name'),0.65,{opacity:0.3, y:0, ease:"Power1.easeOut"});
    },

    slideOutUI: function () {
        $(".product-view-wrapper .product-sku-name,.productLeftArrow,.productRightArrow,.productDetailsButton,.productFB,.productTwitter,.productInstagram").removeClass('fade-in');
      //  if(this.activeSlide == "current")
     //   $('.slideshow-wrapper-'+this.activeSlide+ '> .product-sku-name').removeClass('fade-out');
         this.skuNameFadeOut("current");
       //$('.product-sku-name').removeClass('fade-in');
        TweenLite.to($('.product-sku-name'),0.65,{opacity:0, y:this.$el.height()*-0.5, ease:"Power1.easeOut"});
   //    $('.slideshow-wrapper-'+this.activeSlide+ '> .product-sku-name').removeClass('fade-in-now');
    },

    skuNameFadeIn:function(currentSlide){
     //    $('.slideshow-wrapper-'+currentSlide+ '> .product-sku-name').css({opacity:0});

  //     TweenLite.to(  $('.slideshow-wrapper-'+currentSlide+ '> .product-sku-name'),1,{opacity:0.3});
   //     $('.slideshow-wrapper-'+currentSlide+ '> .product-sku-name').transition({opacity:0.3},1000);
    },
    skuNameFadeOut:function(currentSlide){
    //    $('.slideshow-wrapper-'+currentSlide+ '> .product-sku-name').css({opacity:0});
     //   $('.slideshow-wrapper-'+currentSlide+ '> .product-sku-name').transition({opacity:0.3},1000);
    },
    openHDImage: function () {
        app.productView.slideOutUI();
        $(document).smartpreload({ images: ["assets/img/products/hd/product" + this.tempID + "_" + this.currentAltImage + ".jpg"], onloadall: this.zoomInHDImage });
    },
    zoomInHDImage: function () {
        var me = this;
        app.productView.$el.find('#productZoomImage').attr("src", "assets/img/products/hd/product" + app.productView.tempID + "_" + app.productView.currentAltImage + ".jpg");
        var hdImageWidth = 3000;
        var hdImageHeight = 2001;

        var screenRatio = app.productView.$el.height() / app.productView.$el.width();


        var scaleRatio = app.productView.$el.height() / hdImageHeight;
        if (screenRatio <= hdImageHeight / hdImageWidth)
            scaleRatio = app.productView.$el.width() / hdImageWidth;
        setTimeout(function () {
            var pos = app.productView.panZoomedImage(0, 0, true);

            app.productView.$el.find('.hd-image-wrapper').show();
            app.productView.$el.find('#productZoomImage').css({scale: scaleRatio, x: pos.x, y: pos.y});

            TweenLite.to(app.productView.$el.find('#productZoomImage'), 1, {scale: 1, x: pos.x, y: pos.y, force3D: true,
                onComplete: function () {
                    app.productView.$el.find('.slideshow-wrapper-current').hide();
                    app.productView.bZoomReady = true;
                }});

        }, 1000);
        //  app.productView.$el.find('#productZoomImage').transition({scale:1,x:pos.x, y:pos.y},1000,function(){ app.productView.$el.find('.slideshow-wrapper').hide();app.productView.bZoomReady = true;});
    },
    zoomOutHDImage: function () {
        //  app.productView.$el.find('#productZoomImage').attr("src","assets/img/products/hd/product" +  app.productView.tempID + "_"+ app.productView.currentAltImage+".jpg");
        app.productView.bZoomReady = false;
        var hdImageHeight = 2001;
        var hdImageWidth = 3000;
        var screenRatio = app.productView.$el.height() / app.productView.$el.width();
        var scaleRatio = app.productView.$el.height() / hdImageHeight;
        if (screenRatio <= hdImageHeight / hdImageWidth)
            scaleRatio = app.productView.$el.width() / hdImageWidth;
        //   scaleRatio =    app.productView.$el.width()/hdImageWidth;
        var pos = app.productView.panZoomedImage(0, 0, false);

        app.productView.$el.find('.slideshow-wrapper-current').show();
        //  app.productView.$el.find('#productZoomImage').css({scale:0});
        TweenLite.to(app.productView.$el.find('#productZoomImage'), 1, {scale: scaleRatio, x: pos.x, y: pos.y, force3D: true,
            onComplete: function () {
                app.productView.$el.find('.hd-image-wrapper').hide();
                app.productView.slideInUI();
            }});
        //   app.productView.$el.find('#productZoomImage').transition({scale:scaleRatio,x:pos.x, y:pos.y},1000,function(){app.productView.$el.find('.hd-image-wrapper').hide();});
    },
    
    panZoomedImage: function (mouseX, mouseY, bNow) {


    var wrapperW = app.productView.$el.find('.hd-image-wrapper').width();
    var wrapperH = app.productView.$el.find('.hd-image-wrapper').height();
    var imageW = app.productView.$el.find('#productZoomImage').width();

    var imageH = app.productView.$el.find('#productZoomImage').height();

    if (imageW == 0)  imageW = 3000;
    if (imageH == 0)  imageH = 2001;

    var posX = mouseX * (imageW / 2 - wrapperW / 2) - imageW / 2;
    var posY = mouseY * (imageH / 2 - wrapperH / 2) - imageH / 2;
    if (bNow == true)
        app.productView.$el.find('#productZoomImage').css({x: (posX), y: (posY)});
    else if (bNow == false) {
    }
    else
        TweenLite.to(app.productView.$el.find('#productZoomImage'), 0.5, {x: posX, y: posY, force3D: true});
    //   app.productView.$el.find('#productZoomImage').css({x:(posX),y:(posY)});
    /*
     posX = (app.productView.lastZoomPosX + posX)*0.5;
     posY = (app.productView.lastZoomPosY + posY)*0.5;
     app.productView.lastZoomPosX = posX;
     app.productView.lastZoomPosX = posY;
     */
    var dat = {x: posX, y: posY};


    return dat;
    },

    /** EVENTS **/
    

    pageDotClicked: function (e) {

        this.moveToAltView(e.target.id.replace("productDot", ""));
    },
    onLeftClicked: function (e) {
        var me = this;

        if( this.bSlidePlaying ) return;
        var newID = this.productID - 1;
        if (newID <= 0)
            newID = app.collectionData.length ;

        this.direction = 1;

        if(this.detailsView.bOpened){
         //   this.detailsView.slideOut();
            this.toggleDetailPanel(true);
            //this.slideDetails(false);
            setTimeout(function(){  me.moveToSlide(newID);}, 1500);
        }
        else
          this.moveToSlide(newID);

    },
    onRightClicked: function (e) {
        var me = this;
        if( this.bSlidePlaying ) return;
        console.log(this.productID);
       var newID = this.productID + 1;
        if (newID > app.collectionData.length)
            newID= 1;
        console.log(newID + " " + this.detailsView.bOpened);

        this.direction = -1;
        if(this.detailsView.bOpened){
         //   this.slideDetails(false);
            this.toggleDetailPanel(true);
        //    this.detailsView.slideOut();
            setTimeout(function(){  me.moveToSlide(newID);}, 1500);
        }
        else
            this.moveToSlide(newID);

    },
    onBackToCollection: function () {
        app.productView.slideOutUI();
        this.zoomOut();
        app.collectionView.backFromProductView();
    },
    onBackToHomepage:function(){
        app.productView.slideOutUI();
        this.zoomOut(true);
        app.collectionView.backFromProductView(true);
    },
    onImageClicked: function () {
        this.openHDImage();
    },
    onZoomedImageClicked: function () {
        this.zoomOutHDImage();
    },
    onMouseZoomMove: function (e) {
        if (this.bZoomReady == false) return;
        var wrapperW = app.productView.$el.find('.hd-image-wrapper').width();
        var mouseX = ((e.originalEvent.pageX - 50) / wrapperW - 0.5) * -1;

        var wrapperH = app.productView.$el.find('.hd-image-wrapper').height();
        var mouseY = ((e.originalEvent.pageY - 50) / wrapperH - 0.5) * -1;

        this.panZoomedImage(mouseX, mouseY);
    },
    onDetailsButtonClicked:function(e){
       this.toggleDetailPanel();

    },
    toggleDetailPanel:function(bForced){
        this.currentScrollDelta = 0;
        if(  this.bDetailsSliding ) return;
        if(bForced != undefined) this.bDetailsShown = bForced;
        this.bDetailsSliding = true;

        if(this.bDetailsShown){
            this.$el.find('.productDetailsButton').attr('src','assets/img/ui/productdetails.png');
            TweenLite.to(this.$el.find( '.slideshow-wrapper-current'),1,{y:-this.contentHeight*0*0.2+'px', force3D: true});
            TweenLite.to(this.$el.find( '.product-sku-name'),1,{y:-this.contentHeight*0*0.5+'px' ,opacity:(1-0)*0.3, force3D: true});
            TweenLite.to(  this.detailsView.$el,1,{y:this.contentHeight*(1-0)+'px', force3D: true});

           this.detailsView.slideOut();
            //this.slideDetails(false);
        }
        else{
            this.$el.find('.productDetailsButton').attr('src','assets/img/ui/detailsclose.png');
            this.detailsView.slideIn(this.productID);
            TweenLite.to(this.$el.find( '.slideshow-wrapper-current'),1,{y:-this.contentHeight*1*0.2+'px'});
            TweenLite.to(this.$el.find( '.product-sku-name'),1,{y:-this.contentHeight*1*0.5+'px' ,opacity:(1-1)*0.3});
            TweenLite.to(  this.detailsView.$el,1,{y:this.contentHeight*(1-1)+'px'});
            this.detailsView.slideInComplete();
        }
        this.bDetailsShown = !this.bDetailsShown;
        //
    },
    onResize:function(){
      this.contentHeight = this.$el.height();
    },
    onMouseWheel: function (e) {

        if(this.bSlidePlaying) return;
        var evt = e.originalEvent;

        var delta = evt.wheelDelta || -evt.detail*9;

       // console.log(delta);
        this.currentScrollDelta -= delta;

   //     this.$el.find( '.slideshow-wrapper-current').css({y:});

      //  TweenLite.to(this.$el.find( '.slideshow-wrapper-current'),10,{y:delta*-200+"px"});
        if (this.currentScrollDelta < 0 ){
            this.currentScrollDelta = 0;
            this.$el.find('.productDetailsButton').attr('src','assets/img/ui/productdetails.png');
            /*

             this.$el.find( '.slideshow-wrapper-current').transition({y:parseInt(direction*20)+'%'},1000,'cubic-bezier(0.165, 0.84, 0.44, 1)' );
             //  this.$el.find( '.product-sku-name').transition({y:parseInt(direction*1500)+'%',opacity:0}, 800, 'easeInOutSine');
             this.$el.find( '.product-sku-name').addClass('slide-up');

             */

           // if(this.bDetailsShown)
              //  this.toggleDetailPanel(true);
        }
        else  if (this.currentScrollDelta > 300){
            this.currentScrollDelta = 300;
            this.$el.find('.productDetailsButton').attr('src','assets/img/ui/detailsclose.png');
              //  if( !this.bDetailsShown)
                 //   this.toggleDetailPanel(false);
        }
        var delta = this.currentScrollDelta/300;
        TweenLite.to(this.$el.find( '.slideshow-wrapper-current'),0.65,{y:-this.contentHeight*delta*0.2+'px', force3D: true});
        TweenLite.to(this.$el.find( '.product-sku-name'),0.65,{y:-this.contentHeight*delta*0.5+'px' ,opacity:(1-delta)*0.3, force3D: true});
        TweenLite.to(  this.detailsView.$el,0.65,{y:this.contentHeight*(1-delta)+'px', force3D: true});

        if(delta > 0.5 && this.detailsView.bOpened == false  ){
            this.detailsView.slideInComplete();
        }
       // ;
      //.transition({y:0},650,'cubic-bezier(0.165, 0.84, 0.44, 1)');
        console.log(this.currentScrollDelta + " " + delta +" " + this.contentHeight*delta);
    }


});