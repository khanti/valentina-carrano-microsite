/**
 * Created by piotrostiak on 16/04/14.
 */
var AppView = Backbone.View.extend({
    el:"#AppView",
    bMenuOpened:false,
    events:{
        "mouseover  #menu-button":"onMouseOverMenu",
        "mouseout  #menu-button":"onMouseOutMenu",
        "click #menu-button":"onMenuClicked",
        "resize":"onResize"
    },
    initialize: function(){
        $(window).on('resize', this.onResize)
    },
    onMenuClicked:function(option){


       if(this.bMenuOpened)
       {
           this.bMenuOpened = false;
           app.mainMenu.fadeOut();
           $("#menu-button").removeClass("open");
       }else{
           this.bMenuOpened = true;
           app.mainMenu.fadeIn();
           $("#menu-button").addClass("open");
       }


    },

    forceClose:function(){
        this.bMenuOpened = false;
        app.mainMenu.fadeOut();
        $("#menu-button").removeClass("open");
    },
    onMouseOverMenu:function(){
       // $("#menu-button > i").addClass("hover");

    },
    onMouseOutMenu:function(){
      //  $("#menu-button > i, #menu-button > i::after, #menu-button > i::before").removeClass("hover");
    },
    onResize:function(){
      app.collectionView.onResize();
        app.productView.onResize();
    }
});