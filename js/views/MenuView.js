/**
 * Created by piotrostiak on 14/04/14.
 */
var MenuView = Backbone.View.extend({
    el:"#MenuView",
    events:{
      'click':'closeMenu',
      'click #menuItem1':'linkToCollection',
        'click #menuItem2':'linkToBrand'

    },
    initialize:function(){

    },
    closeMenu:function(){

      app.appView.forceClose();

    },
    fadeIn:function(){
        this.$el.css({'opacity':0/*,scale:0.8*/});
        this.$el.show();
        this.$el.transition({opacity:0.95, scale:1}, 1000, 'easeOutQuad')

        setTimeout(function(){
            $('#menuItem3 .left').addClass('slideIn');
            $('#menuItem3 .right').addClass('slideIn');},
        250);
        setTimeout(function(){
                $('#menuItem2 .left').addClass('slideIn');
                $('#menuItem2 .right').addClass('slideIn');},
            450);
        setTimeout(function(){
                $('#menuItem1 .left').addClass('slideIn');
                $('#menuItem1 .right').addClass('slideIn');},
            650);
    },
    linkToCollection:function(){
        app.collectionView.open();
    },
    linkToBrand:function(){
        window.location.hash = '!brand';
    },
    fadeOut:function(){
        $('.left').removeClass('slideIn');
        $('.right').removeClass('slideIn');
        var me = this;
        this.$el.transition({opacity:0/*,scale:0.8*/}, 500, 'easeInQuad', function(){ me.$el.hide()})
    }
});