/**
 * Created by piotrostiak on 14/04/14.
 */
var SlideshowView = Backbone.View.extend({
    el: "#SlideShow",
    template: Handlebars.compile($("#slideshow_template").html()),
    image: [],
    colors: ["#f00", "#0f0", "#00f", "#ff0", "#f00", "#0f0", "#00f", "#ff0"],
    currentSlide: 0,
    bLoaded:false,
    previousSlide: 0,
    currentScrollDelta: 0,
    lastScrollDelta:0,
    bSlidePlaying: false,
    bMouseDown:false,
    slideAutoPlayTmer:null,
    bFirstSlide:true,
    previousDirection:0,
    events: {
        'click .slideshow-pagination>i': 'onDotClicked',
        'DOMMouseScroll .slideshow-wrapper': 'onMouseWheel',
        'mousewheel .slideshow-wrapper': 'onMouseWheel',
        'touchstart .slideshow-wrapper':'onTouchDragStart',
        'touchend .slideshow-wrapper':'onTouchDragEnd',
        'touchmove .slideshow-wrapper':'onTouchDrag',
        'mousedown .slideshow-wrapper':'onDragStart',
        'mouseup .slideshow-wrapper':'onDragEnd',
        'mousemove .slideshow-wrapper':'onDrag',
        'click #menu-button':'onMenuClicked',
        'click .slide-cta':'ctaClicked'
    },
    initialize: function () {
        for (var i = 1; i <= 8; i++)
            this.image.push("assets/img/slideshow/0" + i + ".jpg");
        _.bindAll(this, 'keyPressed');
        $(document).bind('keydown', this.keyPressed);
        this.render();
    },

    render: function () {
        var self = this;
        this.$el.html(this.template({image: this.image}));

        for (var i = 0; i < this.image.length; i++) {
            $("#slideH" + i).css('color', this.colors[i]);
        }


        $("#slideH0").transition({y: parseInt(-$('.slideshow-wrapper').height() *0.05-128), opacity:0}, 0);
    },
    fadeIn: function () {
        this.showSlide(0);
        app.collectionView.slideOut();
       this.show();
       this.bLoaded = true;
        this.currentSlide = 0;
        this.normalizeDepth(0);
          this.playAutoSlide();
        if(this.bFirstSlide) {
            $("#main-slideshow-wrapper").addClass("zoomIn");
            //      $('#slide' + 0).css({'transform':'translateY(100%)'});
            $('#slide' + 0 + " > .slide-image").transition({scale: '1'}, 5000, 'easeOutQuad');

            $("#slideH0").transition({y: /* parseInt($('.slideshow-wrapper').height() *0.5+128)*/0, opacity: 0.3, delay: 2000}, 1500, 'easeOutQuad');
            $(".slide-cta").transition({top: "70%", opacity: 1, delay: 2600}, 1000);

            //  $('.slideshow-pagination ').transition({right: '28px', delay: 3700}, 1000);


            $('#slideDiamond0').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 0 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond1').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 1 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond2').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 2 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond3').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 3 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond4').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 4 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond5').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 5 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond6').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 6 * 100)}, 150, 'easeOutQuad');
            $('#slideDiamond7').transition({opacity:'1',x:'-10px',y: '-15', delay: parseInt(3700 + 7 * 100)}, 150, 'easeOutQuad');

        }
        else{
         //   $('#slide' +0).css({'transform':'translateY(0)'});
            $('#slide' +0).css({y:'0'});
            this.bFirstSlide = true;
        }

    },
    moveToSlide: function (newSlide, bImidiate) {
        var me = this;
        var direction = 1;
        if(this.bSlidePlaying || newSlide == this.currentSlide) return;

        this.playAutoSlide();
        me.bSlidePlaying = true;

        this.previousSlide = this.currentSlide;
        this.normalizeDepth(newSlide);

        if(this.previousSlide < newSlide)
            direction = 1;
        else
            direction = -1;




        $('#slide' + newSlide).css({'y':+parseInt(direction*100)+'%'});
/*
        if(direction != this.previousDirection){
            $('#slide' + this.previousSlide ).transition({y: '-'+parseInt(direction*50)+'%'}, 1500);
        }
        else if(this.bFirstSlide){
            this.bFirstSlide = false;
            $('#slide' + this.previousSlide ).transition({y: '-'+parseInt(direction*50)+'%'}, 1500);
        }
        else
        */
            $('#slide' + this.previousSlide ).transition({y: parseInt(direction*-50)+'%'}, 1000);

        $('#slideH' + this.previousSlide  ).transition({y: parseInt(direction*-600)+'%'}, 1000);
        $('#slideH' + newSlide  ).css({y:  parseInt(direction*-600)+'%'});

        if(bImidiate == true){
            $('#slideH' + newSlide  ).transition({y: parseInt(0)+'%'}, 0);
            $('#slide' + newSlide).transition({y: 0}, 0, function () {
                me.bSlidePlaying = false;
            },'easeInQuad');
        }
        else{
            $('#slideH' + newSlide  ).transition({y: parseInt(0)+'%'}, 1000);
            $('#slide' + newSlide).transition({y: 0}, 1200, function () {
                me.bSlidePlaying = false;
            },'easeInQuad');
        }


        $('#slide' + newSlide + " > .slide-image").transition({scale: '1.1'},0);
        $('#slide' + newSlide + " > .slide-image").transition({scale: '1'}, 6000, 'easeOutQuad');

        this.previousDirection = direction;
        this.currentSlide = newSlide
        this.setActiveDot(newSlide);
        this.currentScrollDelta = 0;
    },
    playAutoSlide:function(){
      //   return;
        var me = this;
        if ((this.currentSlide + 1) < 8){


            clearTimeout(  me.slideAutoPlayTmer);

              this.slideAutoPlayTmer  =  setTimeout(function(){
                  me.moveToNext();
              },6000);
        }
    },
    normalizeDepth: function (slideID) {
        var me = this;
        $(".slideshow-image").each(function (index) {
            if (index == slideID) {
                $(this).css('z-index', 11);
                $(this).show();
            }

            else if (index == me.previousSlide) {

                $(this).css('z-index', 10);
                $(this).show();
            }

            else {
                $(this).css('z-index', (9 - index));
                $(this).hide();
            }
        });
    },
    showSlide: function (slideID) {
        //  $('.slideshow-image').hide();
        //   $('#slide'+slideID).show();
        $('#slide' + slideID).addClass('active');
        this.setActiveDot(slideID);
    },
    setActiveDot: function (dotIndex) {
        $('.slideshow-pagination > i').removeClass("on");
        $("#slideDiamond" + dotIndex).addClass("on");
    },
    onDotClicked: function (e) {
        if(this.bSlidePlaying) return;

        this.moveToSlide(parseInt(e.target.id.replace("slideDiamond", "")));
        //this.showSlide(e);
    },
    onMouseWheel: function (e) {
        if(this.bSlidePlaying) return;


        var evt = e.originalEvent;

        var delta = evt.wheelDelta || -evt.detail*9;

        this.currentScrollDelta += delta;


        if (this.currentScrollDelta < -500)
           this.moveToNext();
        else  if (this.currentScrollDelta > 500)
           this.moveToPrev();


    },
    moveToNext:function(){
        if ((this.currentSlide + 1) < 8)
            this.moveToSlide(this.currentSlide + 1);
        else{
            app.collectionView.open();
        }
          //  this.moveToSlide(0);
    },
    moveToPrev:function(){
        if ((this.currentSlide - 1) >= 0)
            this.moveToSlide(this.currentSlide - 1);
        else
            this.moveToSlide(7);
    },
    keyPressed:function(e){

      if(e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 87 )
          this.moveToPrev();

        if(e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 83 )
            this.moveToNext();
    },
    onDragStart:function(e){
        this.currentScrollDelta = 0;
        this.lastScrollDelta =  e.originalEvent.pageY;
        this.bMouseDown = true;
    },
    onDragEnd:function(e){
        this.currentScrollDelta = 0;
        this.bMouseDown = false;
    },
    onDrag:function(e){
        e.stopPropagation();
        if(this.bMouseDown == false) return;
        var newPosition = e.originalEvent.pageY;
        var delta = this.lastScrollDelta - newPosition
        this.lastScrollDelta =  newPosition;
        this.currentScrollDelta += delta;

        console.log( this.currentScrollDelta );
        if (this.currentScrollDelta < -100)
            this.moveToNext();
        else  if (this.currentScrollDelta > 100)
            this.moveToPrev();
    },
    onTouchDragStart:function(e){
        this.currentScrollDelta = 0;
        this.lastScrollDelta =  e.originalEvent.touches[0].pageY;
    },
    onTouchDragEnd:function(e){
        this.currentScrollDelta = 0;

    },
    onTouchDrag:function(e){    e.stopPropagation();e.preventDefault();
        var newPosition = e.originalEvent.touches[0].pageY;
        var delta = this.lastScrollDelta - newPosition
        this.lastScrollDelta =  newPosition;
        this.currentScrollDelta += delta;

        console.log( this.currentScrollDelta );
        if (this.currentScrollDelta < -100)
            this.moveToNext();
        else  if (this.currentScrollDelta > 100)
            this.moveToPrev();


    },
    show:function(){
        this.$el.css('visibility','visible');
        this.moveToSlide(0, true);
    },
    hide:function(){
        this.$el.css('visibility','hidden');
    },
    ctaClicked:function(){
        app.collectionView.open();
    }

});