/**
 * Created by piotrostiak on 14/04/14.
 */
var BrandView = Backbone.View.extend({
   el:"#BrandView",
    template: Handlebars.compile($("#details-view-template").html()),
    detailsData:null,
    bOpened:false,
    productID:0,
    initialize:function(){
        this.$el.hide();

    },
    render:function(){
        var me = this;

        this.detailsData = {
            imageURLA:"assets/img/details/details01_a.jpg",
            imageURLB:"assets/img/details/details01_b.jpg",

            titleA:"SOME TITLE COMES HERE "+ this.productID,
            textA:"Lorem dolor psum sit amet fluctuat nec mergitur sic itur ad astra, per angusta ad augusta. Tu quoque mi filii, gaius julius said.",

            titleB:"SOME TITLE COMES HERE" + this.productID,
            textB:"Lorem dolor psum sit amet fluctuat nec mergitur sic itur ad astra, per angusta ad augusta. Tu quoque mi filii, gaius julius said."
        };

        $(document).smartpreload({ images:  [  me.detailsData.imageURLA,  me.detailsData.imageURLB ], onloadall: function () {
            me.$el.html(me.template(me.detailsData));
            if(me.bOpened == false){
                me.$el.css({y:app.productView.$el.height()});
                me.$el.show();

                me.$el.transition({y:0},600,'easeInOutSine',function(){ });
            }
            me.bOpened = true;
            //     me.$el.hide();
        }});



    },
    slideIn:function(productID){
        this.productID = productID;

        this.render();

    },
    slideOut:function(){
        var me = this;
        this.$el.show();
        this.$el.transition({y:app.productView.$el.height()},600,'easeInOutSine', function(){ me.$el.hide();});
        me.bOpened = false;
    }
});