/**
 * Created by piotrostiak on 14/04/14.
 */

var App = Backbone.View.extend({

// /usr/bin/scss
    //C:/Ruby193/bin/scss.bat
    slideshowImages:[],
    imagesCount:0,
    imagesLoaded:0,
    appView:null,
    mainMenu:null,
    collectionView:null,
    productView:null,
    brandView:null,
    collectionData:null,
    bInited:false,
    postLoadURL:"",
    currentView:"",
    initialize: function(){

        document.ontouchmove = function(e) {e.preventDefault();};

        var me = this;
        this.appView = new AppView();
        this.appView.app = app;

        this.mainMenu = new MenuView();
        this.mainMenu.app = app;

        this.productView = new ProductView();
        this.productView.app = app;
        this.brandView = new BrandView();

      for(var i = 1; i <= 8; i++)
          this.slideshowImages.push("assets/img/slideshow/0"+i+".jpg");



        $.getJSON('assets/collection.json', function(data) {
            me.collectionData = data.collection;


            me.prealoadimages();
        });

        $.address.change(function(event) {

            me.postLoadURL = event.value;
            if( me.bInited)
              me.parseURL(event.value);

        });

        $('a').click(function() {

            $.address.value($(this).attr('href').replace(/^#/, ''));
        });
    },
    parseURL:function(value){

        if( app.currentView == "brand" && value.indexOf("brand") == -1 ){
            app.brandView.slideOut();
        }
        if(app.currentView != "collection" && value.indexOf("collection") != -1){
            if(     app.currentView == "product" && app.collectionView.bLoaded) {

                app.productView.onBackToCollection();

            }
            else{
                if(     app.currentView == "product")
                app.productView.onBackToCollection();

                app.collectionView.slideIn();
            }

            app.currentView = "collection";

        }
        else if(value.indexOf("product") != -1){
            if(     app.currentView == "product") return;
            var productID = parseInt(value.replace("/!product/",""));
            app.collectionView.openProductView(productID);
            app.currentView = "product";
        }
        else if(   app.currentView != "brand" && value.indexOf("brand") != -1){
            app.currentView = "brand";
            app.brandView.slideIn();
        }
        else if( app.currentView != "home" && (value == "" || value == "/" || value == "index.html")){

            if(     app.currentView == "product"){
                app.productView.onBackToHomepage();
                if(app.slideShowView.bLoaded){   app.slideShowView.show(); app.currentView = "home";}
                else
                setTimeout(function(){   app.fadeInSlideshow(); app.currentView = "home";}, 1000);
            }
            else if(     app.currentView == "brand"){

                if(app.slideShowView.bLoaded){   app.slideShowView.show(); app.currentView = "home";}
                else
                    setTimeout(function(){   app.fadeInSlideshow(); app.currentView = "home";}, 1000);
            }
            else{
                app.currentView = "home";
                setTimeout(function(){   app.fadeInSlideshow();}, 100);
            }


        }

    },
    prealoadimages:function(){
        var i;
        this.fadeInLoader();
        this.imagesCount = this.slideshowImages.length;
        var imagesToLoad = [];

        for(i = 0; i < this.collectionData.length; i++){
            var imageFile = (i+1);

            if ((i+1) < 10) imageFile = "0" + (i+1);

            imagesToLoad.push("assets/img/collection/" + imageFile + ".jpg");
        }
        for(i = 0; i < this.slideshowImages.length; i++)
            imagesToLoad.push(this.slideshowImages[i]);
        $(document).smartpreload({ images: this.slideshowImages, oneachimageload: this.onimageload/*, onloadall: this.onallimagesloaded*/ });


    },
    onimageload:function(src){

        app.imagesLoaded++;
       $('.logo-mask').transition({'width': (app.imagesLoaded/app.imagesCount)*100+'%'},100,'easeInOutCubic',function(){
           if(app.imagesLoaded == app.imagesCount && !app.bInited){
               app.bInited = true;
               app.onallimagesloaded();
           }

       });



    },
    onallimagesloaded:function(){

        app.collectionView = new CollectionView();
        app.slideShowView = new SlideshowView();
        app.slideShowView.app = app;

        app.fadeOutLoader();
        setTimeout(function(){app.parseURL(app.postLoadURL);},1000);

    },
    fadeInSlideshow:function(){

         this.slideShowView.fadeIn();


    },
    fadeInLoader:function(){
        $('.logo-wrapper').fadeIn();
    },
    fadeOutLoader:function(){
    //    $('.logo-wrapper').fadeOut();
      //  $('#menu-button').transition({y:'-=30px'},0);
        $('.logo-wrapper').transition({y:parseInt(-$('.slideshow-wrapper').height() *0.5 +0)+'px',delay:1000},1000,'easeOutCubic');
        $('#menu-button').transition({y:'+=50px',delay:2000},1500,'easeOutCubic');
    },
    render:function(){


    //    this.$el.html(this.template({who:'Valentina!'}))
    }


});